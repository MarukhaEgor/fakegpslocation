package com.example.fakegpslocation.ui

import android.Manifest
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.fakegpslocation.R
import com.example.fakegpslocation.data.entity.CoordinatesEntity
import com.example.fakegpslocation.service.FakeLocationService
import com.example.fakegpslocation.utils.CheckPermissions
import com.example.fakegpslocation.utils.CheckPermissions.listOfPermissions
import com.example.fakegpslocation.utils.Constants
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_maps.*
import org.koin.android.ext.android.inject


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var mapFragment: SupportMapFragment
    private var mockingCoordinates: CoordinatesEntity = CoordinatesEntity(
        Constants.START_LOCATION_KROP_LATITUDE,
        Constants.START_LOCATION_KROP_LONGITUDE
    )
    private val modelFake: FakeGPSViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        prepareMap()
        setListeners()
    }


    private fun prepareMap() {
        mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val marker = LatLng(
            mockingCoordinates.latitude,
            mockingCoordinates.longitude
        )
        mMap.addMarker(
            MarkerOptions().position(
                marker
            ).title(R.string.start_point_on_map.toString())
        )
        mMap.moveCamera(
            CameraUpdateFactory.newLatLng(
                marker
            )
        )

        modelFake.initDataBase(
            mockingCoordinates.latitude,
            mockingCoordinates.longitude
        )
        mMap.setOnMapClickListener {
            modelFake.onMapClick(it.latitude, it.longitude)
        }
        startObserving()
    }

    private fun setListeners() {
        startButton.setOnClickListener {
            if (checkPermissionsBeforeStart()) {
                if (isMockingServiceRunning(FakeLocationService::class.java)) {
                    stopService()
                }
                startService(mockingCoordinates)
            }
        }
        stopButton.setOnClickListener {
            stopService()
        }
    }

    private fun updateMap(
        it: LatLng?,
    ) {

        mMap.clear()
        mMap.addMarker(
            it?.let { marker -> MarkerOptions().position(marker) }
        )
    }

    private fun checkPermissionsBeforeStart(): Boolean {
        if (!CheckPermissions.check(this, *listOfPermissions)) {
            CheckPermissions.request(
                this,
                Constants.REQUEST_CODE,
                *listOfPermissions
            )
            return false
        }
        return true
    }

    private fun startObserving() {
        modelFake.liveData.observe(this, Observer {
            updateMap(
                LatLng(
                    it.latitude,
                    it.longitude
                )
            )
            mockingCoordinates = CoordinatesEntity(
                it.latitude,
                it.longitude
            )
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Constants.REQUEST_CODE -> {
                val showRationale =
                    this.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)

                if (CheckPermissions.hasAll(permissions, grantResults)) {
                    onPermissionGranted()
                } else if (!showRationale) {
                    onPermissionDeniedPermanently()
                } else {
                    onPermissionDenied()
                }

            }
        }
    }

    private fun onPermissionGranted() {
        Toast.makeText(
            this,
            this.getText(R.string.permissions_granted),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun onPermissionDenied() {
        Toast.makeText(
            this,
            this.getText(R.string.permissions_denied),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun onPermissionDeniedPermanently() {
        Snackbar.make(
            this.startButton,
            this.getText(R.string.snackbar_pernissions_label),
            Snackbar.LENGTH_INDEFINITE
        )
            .setAction(this.getText(R.string.snackbar_pernissions_action)) {
                startActivity(
                    Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.fromParts("package", this.packageName, null)
                    )
                )
            }
            .show()
    }

    private fun isMockingServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    private fun startService(coordinates: CoordinatesEntity?) {
        val intent = Intent(this, FakeLocationService::class.java)
        intent.putExtra(Constants.COORDINATE_LATITUDE, coordinates?.latitude)
        intent.putExtra(Constants.COORDINATE_LONGITUDE, coordinates?.longitude)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            (applicationContext).startForegroundService(intent)
        } else {
            (applicationContext).startService(intent)
        }
    }

    private fun stopService() {
        stopService(Intent(this, FakeLocationService::class.java))
    }

    override fun onDestroy() {
        super.onDestroy()
        mapFragment.onDestroy()
    }


}