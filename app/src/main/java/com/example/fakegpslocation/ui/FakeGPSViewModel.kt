package com.example.fakegpslocation.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.fakegpslocation.arch.CoroutineViewModel
import com.example.fakegpslocation.data.Repository
import com.example.fakegpslocation.data.entity.CoordinatesEntity
import kotlinx.coroutines.launch

class FakeGPSViewModel(private val repositoryModel: Repository) : CoroutineViewModel() {

    val _liveData = MutableLiveData<CoordinatesEntity>()

    val liveData: LiveData<CoordinatesEntity>
        get() = _liveData

    fun initDataBase(latitude: Double, longitude: Double){
        scope.launch {
            repositoryModel.initDataBase(CoordinatesEntity(latitude, longitude))
        }
    }

    private fun addCoordinates(latitude: Double, longitude: Double) {
        scope.launch {
            repositoryModel.addCoordinates(CoordinatesEntity(latitude, longitude))
        }
    }


    fun onMapClick(latitude: Double, longitude: Double) {
        addCoordinates(latitude, longitude)
        scope.launch {
            _liveData.postValue(getCoordinates())
        }
    }


    private fun getCoordinates(): CoordinatesEntity? {
        return repositoryModel.getCoordinates()
    }
}