package com.example.fakegpslocation.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.fakegpslocation.data.dao.CoordinatesDao
import com.example.fakegpslocation.data.entity.CoordinatesEntity
import com.example.fakegpslocation.utils.Constants

@Database(
    version = 1,
    entities = [CoordinatesEntity::class ]
)
abstract class CoordinatesDataBase : RoomDatabase() {
    abstract fun coordinatesDao(): CoordinatesDao

    companion object{

        fun getDataBase(context: Context) : CoordinatesDataBase {
            synchronized(this) {
                return Room.databaseBuilder(
                    context.applicationContext,
                    CoordinatesDataBase::class.java,
                    Constants.DATABASE_NAME
                ).build()
            }
        }
    }
}