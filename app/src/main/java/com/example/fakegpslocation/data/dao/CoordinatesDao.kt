package com.example.fakegpslocation.data.dao

import androidx.room.*
import com.example.fakegpslocation.data.entity.CoordinatesEntity

@Dao
interface CoordinatesDao {
    @Query("SELECT * FROM CoordinatesEntity")
    fun getCoordinates(): List<CoordinatesEntity?>?
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(coordinates: CoordinatesEntity)
    @Delete
    fun delete(coordinates: CoordinatesEntity?)
}