package com.example.fakegpslocation.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CoordinatesEntity(
    @PrimaryKey
    var latitude: Double,
    var longitude: Double
)
