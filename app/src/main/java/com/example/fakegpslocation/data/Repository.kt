package com.example.fakegpslocation.data


import com.example.fakegpslocation.data.database.CoordinatesDataBase
import com.example.fakegpslocation.data.entity.CoordinatesEntity
import org.koin.core.KoinComponent

class Repository(private val dataBase: CoordinatesDataBase) : KoinComponent {
    
    fun initDataBase(coordinates: CoordinatesEntity){
        dataBase.coordinatesDao().insert(coordinates)
    }

    fun addCoordinates(coordinates: CoordinatesEntity) {
        dataBase.coordinatesDao().insert(coordinates)
        dataBase.coordinatesDao().delete(dataBase.coordinatesDao().getCoordinates()?.first())
    }

    fun getCoordinates(): CoordinatesEntity? {
        return dataBase.coordinatesDao().getCoordinates()?.last()
    }

}