package com.example.fakegpslocation.utils

object Constants {
    const val ID = 1
    const val CHANNEL_ID = "GPSLocationService Notification"
    const val CHANNEL_NAME = "Notification"
    const val NOTIFICATION_CHANNEL_DESCRIPTION = "Personal notifications"
    const val START_LOCATION_KROP_LATITUDE = 48.5064266
    const val START_LOCATION_KROP_LONGITUDE = 32.2597293
    const val DATABASE_NAME = "fakeGPSLocation database"
    const val COORDINATE_LATITUDE = "latitude"
    const val COORDINATE_LONGITUDE = "longitude"
    const val TIMER_NAME = "GPSTimer"
    const val TIMER_PERIOD: Long = 500
    const val LOCATION_ACCURACY = 5f
    const val LOCATION_SPEED = 10f
    const val TIMER_DELAY: Long = 0
    const val REQUEST_CODE = 414
}