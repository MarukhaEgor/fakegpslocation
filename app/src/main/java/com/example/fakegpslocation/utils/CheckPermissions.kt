package com.example.fakegpslocation.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import com.example.fakegpslocation.ui.MapsActivity

object CheckPermissions {

    val listOfPermissions = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
    )

    fun request(activity : MapsActivity, requestCode: Int, vararg listOfPermissions: String?) {
        activity.requestPermissions(listOfPermissions, requestCode)
    }

    fun check(ctx: Context?, vararg listOfPermissions: String?): Boolean {
        for (permission in listOfPermissions) {
            if (ctx?.let { ContextCompat.checkSelfPermission(it, permission!!) }
                != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

    fun hasAll(permissions: Array<String>, grantResults: IntArray): Boolean {
        if (permissions.size != grantResults.size) return false
        for (i in permissions.indices) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }
}