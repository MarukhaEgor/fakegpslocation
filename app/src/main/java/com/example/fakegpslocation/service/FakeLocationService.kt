package com.example.fakegpslocation.service


import android.Manifest
import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.location.LocationManager.GPS_PROVIDER
import android.os.Build
import android.os.IBinder
import android.os.SystemClock
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import com.example.fakegpslocation.R
import com.example.fakegpslocation.ui.MapsActivity
import com.example.fakegpslocation.utils.Constants
import com.google.android.gms.location.FusedLocationProviderClient
import java.util.Timer
import kotlin.concurrent.scheduleAtFixedRate


class FakeLocationService : Service() {

    private var timer = Timer(Constants.TIMER_NAME, true)

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            createNotification()
            createFakeLocation(intent)
        }
        return super.onStartCommand(intent, flags, startId)
    }


    private fun createNotification() {

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val channel = NotificationChannel(
            Constants.CHANNEL_ID, Constants.CHANNEL_NAME,
            NotificationManager.IMPORTANCE_HIGH
        )

        val intent_main_activity =
            Intent(this.applicationContext, MapsActivity::class.java)


        val pending_intent_main_activity = PendingIntent.getActivity(
            this.applicationContext, Constants.REQUEST_CODE,
            intent_main_activity, 0
        )

        channel.description =
            Constants.NOTIFICATION_CHANNEL_DESCRIPTION
        notificationManager.createNotificationChannel(channel)
        val builder = NotificationCompat.Builder(this, Constants.CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(applicationContext.getText(R.string.title_notification))
            .setContentText(applicationContext.getText(R.string.text_notification).toString())
            .setContentIntent(pending_intent_main_activity)
            .build()
        val notify_manager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notify_manager.notify(Constants.ID, builder)
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            startForeground(Constants.ID, builder)
        }

    }



    private fun createFakeLocation(intent: Intent) {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val locationProvider = FusedLocationProviderClient(applicationContext)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }
        locationProvider.setMockMode(true)
        locationManager.addTestProvider(
            GPS_PROVIDER,
            false, false, false,
            false, true, true, true, 1, 5
        )
        locationManager.setTestProviderEnabled(GPS_PROVIDER, true)

        val mockLocation = Location(GPS_PROVIDER)
        runProvideLocation(mockLocation, locationManager, locationProvider, intent)
    }

    private fun runProvideLocation(
        mockLocation: Location, locationManager: LocationManager,
        locationProvider: FusedLocationProviderClient, intent: Intent
    ) {
        val latitude = intent.getDoubleExtra(Constants.COORDINATE_LATITUDE, Constants.START_LOCATION_KROP_LATITUDE)
        val longitude = intent.getDoubleExtra(Constants.COORDINATE_LONGITUDE, Constants.START_LOCATION_KROP_LONGITUDE)
        timer.scheduleAtFixedRate(Constants.TIMER_DELAY, Constants.TIMER_PERIOD) {
            mockLocation.time = System.currentTimeMillis()
            mockLocation.accuracy = Constants.LOCATION_ACCURACY
            mockLocation.speed = Constants.LOCATION_SPEED
            mockLocation.latitude = latitude
            mockLocation.longitude = longitude
            mockLocation.elapsedRealtimeNanos = SystemClock.elapsedRealtimeNanos()
            locationManager.setTestProviderLocation(GPS_PROVIDER, mockLocation)
            locationProvider.setMockLocation(mockLocation)
        }
    }


    override fun onDestroy() {
        timer.cancel()
        super.onDestroy()
    }

}