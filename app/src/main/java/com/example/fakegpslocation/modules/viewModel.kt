package com.example.fakegpslocation.modules

import com.example.fakegpslocation.ui.FakeGPSViewModel
import org.koin.dsl.module

val viewModel = module {
    single { FakeGPSViewModel(get()) }
}