package com.example.fakegpslocation.modules


import com.example.fakegpslocation.data.database.CoordinatesDataBase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dbModule  = module {
    single { CoordinatesDataBase.getDataBase(androidContext()) }
}