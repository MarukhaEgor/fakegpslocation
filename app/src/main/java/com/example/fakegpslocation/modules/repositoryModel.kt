package com.example.fakegpslocation.modules

import com.example.fakegpslocation.data.Repository


import org.koin.dsl.module

val repositoryModel = module {
    single { Repository(get()) }
}