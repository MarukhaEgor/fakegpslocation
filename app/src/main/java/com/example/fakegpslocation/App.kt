package com.example.fakegpslocation

import android.app.Application
import com.example.fakegpslocation.modules.appModules
import com.example.fakegpslocation.modules.dbModule
import com.example.fakegpslocation.modules.repositoryModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class App : Application(){
    override fun onCreate() {
        super.onCreate()
        koinStart()
    }

    private fun koinStart() {
        startKoin {
            androidContext(this@App)
            modules(appModules)
        }
    }
}